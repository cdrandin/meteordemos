var require = meteorInstall({"imports":{"ui":{"components":{"hello":{"hello.html":["./template.hello.js",function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// imports/ui/components/hello/hello.html                                                //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
module.exports = require("./template.hello.js");                                         // 1
                                                                                         // 2
///////////////////////////////////////////////////////////////////////////////////////////

}],"template.hello.js":function(){

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// imports/ui/components/hello/template.hello.js                                         //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
                                                                                         // 1
Template.__checkName("hello");                                                           // 2
Template["hello"] = new Template("Template.hello", (function() {                         // 3
  var view = this;                                                                       // 4
  return [ HTML.Raw("<button>Click Me</button>\n  "), HTML.P("You've pressed the button ", Blaze.View("lookup:counter", function() {
    return Spacebars.mustache(view.lookup("counter"));                                   // 6
  }), " times.") ];                                                                      // 7
}));                                                                                     // 8
                                                                                         // 9
///////////////////////////////////////////////////////////////////////////////////////////

},"hello.js":["./hello.html",function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// imports/ui/components/hello/hello.js                                                  //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
module.import('./hello.html');                                                           // 1
                                                                                         //
Template.hello.onCreated(function () {                                                   // 3
  function helloOnCreated() {                                                            // 3
    // counter starts at 0                                                               // 4
    this.counter = new ReactiveVar(0);                                                   // 5
  }                                                                                      // 6
                                                                                         //
  return helloOnCreated;                                                                 // 3
}());                                                                                    // 3
                                                                                         //
Template.hello.helpers({                                                                 // 8
  counter: function () {                                                                 // 9
    function counter() {                                                                 // 8
      return Template.instance().counter.get();                                          // 10
    }                                                                                    // 11
                                                                                         //
    return counter;                                                                      // 8
  }()                                                                                    // 8
});                                                                                      // 8
                                                                                         //
Template.hello.events({                                                                  // 14
  'click button': function () {                                                          // 15
    function clickButton(event, instance) {                                              // 14
      // increment the counter when button is clicked                                    // 16
      instance.counter.set(instance.counter.get() + 1);                                  // 17
    }                                                                                    // 18
                                                                                         //
    return clickButton;                                                                  // 14
  }()                                                                                    // 14
});                                                                                      // 14
///////////////////////////////////////////////////////////////////////////////////////////

}]},"info":{"info.html":["./template.info.js",function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// imports/ui/components/info/info.html                                                  //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
module.exports = require("./template.info.js");                                          // 1
                                                                                         // 2
///////////////////////////////////////////////////////////////////////////////////////////

}],"template.info.js":function(){

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// imports/ui/components/info/template.info.js                                           //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
                                                                                         // 1
Template.__checkName("info");                                                            // 2
Template["info"] = new Template("Template.info", (function() {                           // 3
  var view = this;                                                                       // 4
  return [ HTML.Raw("<h2>Learn Meteor!</h2>\n  "), HTML.UL("\n    ", HTML.Raw('<li>\n      <form class="info-link-add">\n        <input type="text" name="title" placeholder="Title" required="">\n        <input type="url" name="url" placeholder="Url" required="">\n        <input type="submit" name="submit" value="Add new link">\n      </form>\n    </li>'), "\n    ", Blaze.Each(function() {
    return Spacebars.call(view.lookup("links"));                                         // 6
  }, function() {                                                                        // 7
    return [ "\n      ", HTML.LI(HTML.A({                                                // 8
      href: function() {                                                                 // 9
        return Spacebars.mustache(view.lookup("url"));                                   // 10
      },                                                                                 // 11
      target: "_blank"                                                                   // 12
    }, Blaze.View("lookup:title", function() {                                           // 13
      return Spacebars.mustache(view.lookup("title"));                                   // 14
    }))), "\n    " ];                                                                    // 15
  }), "\n  ") ];                                                                         // 16
}));                                                                                     // 17
                                                                                         // 18
///////////////////////////////////////////////////////////////////////////////////////////

},"info.js":["/imports/api/links/links.js","meteor/meteor","./info.html",function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// imports/ui/components/info/info.js                                                    //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
var Links;module.import('/imports/api/links/links.js',{"Links":function(v){Links=v}});var Meteor;module.import('meteor/meteor',{"Meteor":function(v){Meteor=v}});module.import('./info.html');
                                                                                         // 2
                                                                                         // 3
                                                                                         //
Template.info.onCreated(function () {                                                    // 5
  Meteor.subscribe('links.all');                                                         // 6
});                                                                                      // 7
                                                                                         //
Template.info.helpers({                                                                  // 9
  links: function () {                                                                   // 10
    function links() {                                                                   // 9
      return Links.find({});                                                             // 11
    }                                                                                    // 12
                                                                                         //
    return links;                                                                        // 9
  }()                                                                                    // 9
});                                                                                      // 9
                                                                                         //
Template.info.events({                                                                   // 15
  'submit .info-link-add': function () {                                                 // 16
    function submitInfoLinkAdd(event) {                                                  // 15
      event.preventDefault();                                                            // 17
                                                                                         //
      var target = event.target;                                                         // 19
      var title = target.title;                                                          // 20
      var url = target.url;                                                              // 21
                                                                                         //
      Meteor.call('links.insert', title.value, url.value, function (error) {             // 23
        if (error) {                                                                     // 24
          alert(error.error);                                                            // 25
        } else {                                                                         // 26
          title.value = '';                                                              // 27
          url.value = '';                                                                // 28
        }                                                                                // 29
      });                                                                                // 30
    }                                                                                    // 31
                                                                                         //
    return submitInfoLinkAdd;                                                            // 15
  }()                                                                                    // 15
});                                                                                      // 15
///////////////////////////////////////////////////////////////////////////////////////////

}]}},"layouts":{"body":{"body.html":["./template.body.js",function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// imports/ui/layouts/body/body.html                                                     //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
module.exports = require("./template.body.js");                                          // 1
                                                                                         // 2
///////////////////////////////////////////////////////////////////////////////////////////

}],"template.body.js":function(){

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// imports/ui/layouts/body/template.body.js                                              //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
                                                                                         // 1
Template.__checkName("App_body");                                                        // 2
Template["App_body"] = new Template("Template.App_body", (function() {                   // 3
  var view = this;                                                                       // 4
  return Blaze._TemplateWith(function() {                                                // 5
    return {                                                                             // 6
      template: Spacebars.call(view.lookup("main"))                                      // 7
    };                                                                                   // 8
  }, function() {                                                                        // 9
    return Spacebars.include(function() {                                                // 10
      return Spacebars.call(Template.__dynamic);                                         // 11
    });                                                                                  // 12
  });                                                                                    // 13
}));                                                                                     // 14
                                                                                         // 15
///////////////////////////////////////////////////////////////////////////////////////////

},"body.js":["./body.html",function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// imports/ui/layouts/body/body.js                                                       //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
module.import('./body.html');                                                            // 1
///////////////////////////////////////////////////////////////////////////////////////////

}]}},"pages":{"home":{"home.html":["./template.home.js",function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// imports/ui/pages/home/home.html                                                       //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
module.exports = require("./template.home.js");                                          // 1
                                                                                         // 2
///////////////////////////////////////////////////////////////////////////////////////////

}],"template.home.js":function(){

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// imports/ui/pages/home/template.home.js                                                //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
                                                                                         // 1
Template.__checkName("App_home");                                                        // 2
Template["App_home"] = new Template("Template.App_home", (function() {                   // 3
  var view = this;                                                                       // 4
  return [ Spacebars.include(view.lookupTemplate("hello")), "\n  ", Spacebars.include(view.lookupTemplate("info")), HTML.Raw('\n  <br>\n  <img src="http://201.105.75.151:81/videostream.cgi?user=admin&amp;pwd=">\n  <img src="https://i.imgur.com/dufVgMI.jpg">') ];
}));                                                                                     // 6
                                                                                         // 7
///////////////////////////////////////////////////////////////////////////////////////////

},"home.js":["./home.html","../../components/hello/hello.js","../../components/info/info.js",function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// imports/ui/pages/home/home.js                                                         //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
module.import('./home.html');module.import('../../components/hello/hello.js');module.import('../../components/info/info.js');
                                                                                         //
                                                                                         // 3
                                                                                         // 4
///////////////////////////////////////////////////////////////////////////////////////////

}]},"not-found":{"not-found.html":["./template.not-found.js",function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// imports/ui/pages/not-found/not-found.html                                             //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
module.exports = require("./template.not-found.js");                                     // 1
                                                                                         // 2
///////////////////////////////////////////////////////////////////////////////////////////

}],"template.not-found.js":function(){

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// imports/ui/pages/not-found/template.not-found.js                                      //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
                                                                                         // 1
Template.__checkName("App_notFound");                                                    // 2
Template["App_notFound"] = new Template("Template.App_notFound", (function() {           // 3
  var view = this;                                                                       // 4
  return HTML.Raw('<div id="not-found">\n    <div class="not-found-image">\n      <img src="/img/404.svg" alt="">\n    </div>\n    <div class="not-found-title">\n      <h1>Sorry, that page doesn\'t exist</h1>\n      <a href="/" class="gotohomepage">Go to home</a>\n    </div>\n  </div>');
}));                                                                                     // 6
                                                                                         // 7
///////////////////////////////////////////////////////////////////////////////////////////

},"not-found.js":["./not-found.html",function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// imports/ui/pages/not-found/not-found.js                                               //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
module.import('./not-found.html');                                                       // 1
///////////////////////////////////////////////////////////////////////////////////////////

}]}}},"api":{"links":{"links.js":["meteor/mongo",function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// imports/api/links/links.js                                                            //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
module.export({Links:function(){return Links}});var Mongo;module.import('meteor/mongo',{"Mongo":function(v){Mongo=v}});// Definition of the links collection
                                                                                         //
                                                                                         // 3
                                                                                         //
var Links = new Mongo.Collection('links');                                               // 5
///////////////////////////////////////////////////////////////////////////////////////////

}]}},"startup":{"both":{"index.js":function(){

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// imports/startup/both/index.js                                                         //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
// Import modules used by both client and server through a single index entry point      // 1
// e.g. useraccounts configuration file.                                                 // 2
///////////////////////////////////////////////////////////////////////////////////////////

}},"client":{"index.js":["./routes.js",function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// imports/startup/client/index.js                                                       //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
module.import('./routes.js');// Import client startup through a single index entry point
                                                                                         //
                                                                                         // 3
///////////////////////////////////////////////////////////////////////////////////////////

}],"routes.js":["meteor/kadira:flow-router","meteor/kadira:blaze-layout","../../ui/layouts/body/body.js","../../ui/pages/home/home.js","../../ui/pages/not-found/not-found.js",function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// imports/startup/client/routes.js                                                      //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
var FlowRouter;module.import('meteor/kadira:flow-router',{"FlowRouter":function(v){FlowRouter=v}});var BlazeLayout;module.import('meteor/kadira:blaze-layout',{"BlazeLayout":function(v){BlazeLayout=v}});module.import('../../ui/layouts/body/body.js');module.import('../../ui/pages/home/home.js');module.import('../../ui/pages/not-found/not-found.js');
                                                                                         // 2
                                                                                         //
// Import needed templates                                                               // 4
                                                                                         // 5
                                                                                         // 6
                                                                                         // 7
                                                                                         //
// Set up all routes in the app                                                          // 9
FlowRouter.route('/', {                                                                  // 10
  name: 'App.home',                                                                      // 11
  action: function () {                                                                  // 12
    function action() {                                                                  // 10
      BlazeLayout.render('App_body', { main: 'App_home' });                              // 13
    }                                                                                    // 14
                                                                                         //
    return action;                                                                       // 10
  }()                                                                                    // 10
});                                                                                      // 10
                                                                                         //
FlowRouter.notFound = {                                                                  // 17
  action: function () {                                                                  // 18
    function action() {                                                                  // 17
      BlazeLayout.render('App_body', { main: 'App_notFound' });                          // 19
    }                                                                                    // 20
                                                                                         //
    return action;                                                                       // 17
  }()                                                                                    // 17
};                                                                                       // 17
///////////////////////////////////////////////////////////////////////////////////////////

}]}}},"client":{"main.js":["/imports/startup/client","/imports/startup/both",function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// client/main.js                                                                        //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
module.import('/imports/startup/client');module.import('/imports/startup/both');// Client entry point, imports all client code
                                                                                         //
                                                                                         // 3
                                                                                         // 4
///////////////////////////////////////////////////////////////////////////////////////////

}]}},{"extensions":[".js",".json",".html",".less"]});
require("./client/main.js");