To replicates the environment used to create the Meteor apps I did the following...

`meteor create --release 1.3 Meteor1.3`
`meteor create --release 1.3.2.4 Meteor1.3.2.4`
`meteor create --release 1.3.5.1 Meteor1.3.5.1`
`meteor create --release 1.4.2.3 Meteor1.4.2.3`

then I insert the following html 

`<img src="http://195.208.255.162/mjpg/video.mjpg?COUNTER"/>
 <img src="https://i.imgur.com/dufVgMI.jpg"/>
`

into the rendering portion of the app. Usually, `../MeteorDemos/Meteorx.x.x.x/client/main.html`.

`meteor run` to test in browser. It should work.

Now to build an apk:

`meteor add-platform` then building 

`meteor build ../build_1_3 --server=yourapp.meteor.com --debug`
`meteor build ../build_1_3_2_4 --server=yourapp.meteor.com --debug`
`meteor build ../build_1_3_5_1 --server=yourapp.meteor.com --debug`
`meteor build ../build_1_4_2_3 --server=yourapp.meteor.com --debug`

Fails to render on mobile when building standalone.